  <nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
      <!-- Toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <?php $logo = get_field('logo'); ?>
        <a class="navbar-brand scroll-to" href="#top"><img class="img-responsive" src="<?=$logo?>" style="height: 74px; margin-top: -15px;" alt="Car|Rental"></a>
      </div>
      <!-- Collect the nav links, for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <!-- Nav-Links start -->
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="#" class="scroll-to">Inicio</a></li>
          <li><a href="#services" class="scroll-to">Servicios</a></li>
          <li><a href="#vehicles" class="scroll-to">Modelos</a></li>
          <li><a href="#reviews" class="scroll-to">Calificaciones</a></li>
          <li><a href="#locations" class="scroll-to">Ubicaciones</a></li>
          <li><a href="#contact" class="scroll-to">Contacto</a></li>
        </ul>
        <!-- Nav-Links end -->
      </div>
    </div>
  </nav>