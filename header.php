<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Corporación Interoamericano</title>

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Bootstrap -->
  <link href="css/animate.css" rel="stylesheet">

  <!-- Google Font Lato -->
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="css/font-awesome.min.css">

  <!-- Plugin Styles -->
  <link href="css/datepicker.css" rel="stylesheet">

  <!-- Main Styles -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" type="text/css" media="all" />
  <!-- Available main styles: styles-red.css, styles-green.css -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <link href="css/ie8fix.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:900' rel='stylesheet' type='text/css'>
    <![endif]-->


    <!-- Fav and touch icons -->
    <?php $icono = get_field('icono'); ?>
    <link rel="apple-touch-icon" sizes="180x180" href="<?=$icono?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=$icono?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=$icono?>">
    <link rel="manifest" href="/img/ico/favicon/site.webmanifest">
    <link rel="mask-icon" href="/img/ico/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="<?=$icono?>">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/img/ico/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <?php wp_head();?>
  </head>
  <body id="top" data-spy="scroll" data-target=".navbar" data-offset="260">

    <!-- Header start -->
    <header data-spy="affix" data-offset-top="39" data-offset-bottom="0" class="large">

      <div class="row container box">
        <div class="col-md-5">
          <!-- Logo start -->
          <div class="brand">
            <?php $logo = get_field('logo'); ?>
            <h1><a class="scroll-to" href="#top"><img class="img-responsive" src="<?=$logo?>" alt="Intera Corp." style="margin-top: -4px;"></a></h1>
          </div>
          <!-- Logo end -->
        </div>

        <div class="col-md-7">
          <div class="pull-right">
            <div class="header-info pull-right">
              <div class="contact pull-left">CONTACT:</div>
                <?php $contact = get_field('contacto'); ?>
                <div style="display: inline-block; margin-top: 13.5px; margin-left: 10px;"><a href="tel:<?=$contact?>"><?=$contact?></a></div>
              <!-- Language Switch start -->
              <div style="display:none; opacity:0; width:0; height:0" class="language-switch pull-right">
                <div class="dropdown">
                  <a data-toggle="dropdown" href="#" id="language-switch-dropdown">Select your language </a>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="language-switch-dropdown">
                    <li><a href="#"><img src="img/flags/United-States.png" alt="usa"> English</a></li>
                    <li><a href="#"><img src="img/flags/Germany.png" alt="germany"> German</a></li>
                    <li><a href="#"><img src="img/flags/France.png" alt="france"> French</a></li>
                  </ul>
                </div>
              </div>
              <!-- Language Switch end -->
            </div>
          </div>

          <div class="clearfix"></div>

          <!-- start navigation -->
			<?php get_template_part('navigation'); ?>
          <!-- end navigation -->
        </div>
      </div>

    </header>
    <!-- Header end -->